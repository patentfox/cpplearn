## C++ Learning Notes

`<limit>` header contains limits in C++ (not climits), it contains a templatized class numeric_limits, where limits for numeric classes specified as template parameters can be retrieved.

---
find and delete all executable files, with permission 755  
`$find . -type f -perm 755 | xargs rm`
> https://www.tecmint.com/35-practical-examples-of-linux-find-command/

---
`static_assert` is used to test assertions at compile time. Hence, any conditions passed to it should be verifiable at compile time. If unverifiable at compile time, a build error occurs. It is a *keyword*, doesn't require any header.  
`static_assert(2 > 5, "Not possible")`

---
*free standing* vs *hosted* C++ implementation. Free standing if only a minimal set of libraries as specified by standard are provided, example on some embedded platforms. Hosted implemetation provides a full set of libraries specified in standard.

---
#### brace initialization
*brace initializaton* does not allow *narrowing*. `=` is generally redundant.  
```cpp
int a {10};     // OK
int b {10.5};   // Error, due to narrowing
int c(10.5);    // OK, with warning for narrowing
bool b2 = {1};    // OK, 1 == true
bool b3 {2};    // Error, narrowing
```
Unless a constructor accepting *initializer-list* is present for a type, using `{}` to construct object is same as using `()` to invoke constructor, taking any number of parameters. Example
```cpp
vector<int> v(5, 15);       (1)  
print(v.begin(), v.end());  // 15 printed 5 times
vector<int> u{10,10};       (2)
print(u.begin(), u.end());  // 10 10
```
Since `vector` accepts initializer list as a constructor argument, that version is invoked in (2)

---
Pointer can be implicitly converted to *bool*. Non-null pointer converts to *true*

---
Class template cannot infer template type based on the parameter, as opposed to function template, which can
```cpp
template <typename T>
T square(T val) {
    return val*val;
}
square<int>(2);
square(2.5);    // also OK, double template type inferred

template<typename T>
class MyVector {
    // ....
};
/* template type required, even if constructor requires template type argument */
MyVector<int> vec;  
```
---
`std::copy(src_begin, src_end, dest_begin)` works on any iterators, or even plain C pointers.

`std::fill_n(start, size, val)` initializes `size` number of elements starting from `start` to `val`. Works on plain pointers as well.

`vector` - `at()` method throws `out_of_range` error for index out or range, however, `[]` access does not provide range check.  
`vector` also provides a `swap` method which takes another vector as argument and swaps the contents of 2 vector containers. This is an efficient implementation which swaps the underlying storage.

---
Writing a generic print() function
```cpp
template <typename Iter>
void print(Iter begin, Iter end) {
    while(begin != end) {
        cout << *begin << " ";
        ++begin;
    }
    cout << endl;
}
```
This works as `Iter` can refer to any type like `list<int>` or `vector<int>`. *We don't need to specify generic container type and template parameter type separately*.

---
```cpp
list<int> lst {1,2,3,4};
list<int>::iterator it = find(lst.begin(), lst.end(), 3);
lst.insert(it, 10);
print(lst.begin(), lst.end());  // 1 2 10 3 4
cout << *it << endl;            // 3
it = lst.erase(it);
print(lst.begin(), lst.end());  // 1 2 10 4
cout << *it << endl;            // 4
--it;
cout << *it << endl;            // 10
```
`insert` inserts before the iterator.  
`erase` deletes the element at current iterator. It returns the next valid iterator after the one deleted. `erase` invalidates all the existing iterators including and after the ones erased. *Always use the iterator returned from `erase` method*.

---
```cpp
list<int> lst {1,2,3,4};
list<int>::iterator it1 = lst.find(lst.begin(), lst.end(), 3),
                    it2 = lst.end();
list<int> lst2;
lst2.splice(lst2.begin(), lst, it1, it2);
print(lst.begin(), lst.end());                      // 1 2
print(lst2.begin(), lst2.end());                    // 3 4
```
`splice` is very efficient on `list` container. `splice` removes elements from `it1` to `it2` from `lst` and inserts them into `lst2` before the iterator specified as 1st argument.  
`list` does not provide `[]` operator, as it does not support efficient random access.

---
```cpp
array<int, 2> a1 {{1,2}};
print(a1.begin(), a1.end());
cout << a1.size() << endl;
```
`array` is an alternative to c-style arrays in C++ world. Size is a part of type. Hence, `array<int,2>` and `array<int,3>` are totally different types.

Why do we use double braces while defining a1?  
This was required in early C++11 compilers due to a bug.

---
Creating a C-style string constant
```cpp
// In Header
const char* NAME = "MyName";            // (1) ERROR
const char* const NAME = "MyName";      // (2) FINE
```
In case (1), the including CPP file can change the NAME identifier to point to something else. Also, if multiple CPP files (compilation units) include this, there will be multiple definition error by linker.

In case (2), however, `NAME` is a *constant pointer*, that is, the pointer itself is also constant. `const` variables defined at file scope have **internal linkage** by default, unless they are `volatile` or specifically declared as `extern`. As an optimization, compiler internally creates only one copy. So including it in multiple files is efficient as well.
```cpp
NAME[1] = 'U';  // ERROR
```

`constexpr` when applied to a function, indicates that the compiler will *try* to produce a result at compile time. It can produce a `constexpr` result only if all the arguments are also `constexpr` values or expressions.
```cpp
constexpr int cfun(int x) {
    return x+1;
}

constexpr int result1 = cfun(10);   // OK
int v = 10;
constexpr int result2 = cfun(v);   // ERROR: v not known at compile time
int result3 = cfun(v);             // OK: return value is not required to be constexpr
```
Result of `cfun` will be computed at compile time if the argument `x` is known at compile time.  
`constexpr` cannot be applied to function parameters.

`nullptr` can be used to initialize pointers without an initial value. `nullptr` also tests equal for `NULL` and vice versa, so using `nullptr` is safe.
```cpp
A *pa = nullptr;
A *p2 = NULL;
if (pa == NULL) {
    cout << "is NULL\n";			
}
if (p2 == nullptr) {
    cout << "is nullptr p2\n";
}
```

### Standard Exceptions
defined in `<stdexcept>`  
`length_error` where invalid length is specified (-ve instead of positive)  
`out_of_range` on out of range access for checked accessors.

Implicit conversion is applicable to user defined types as well.
```cpp
class complex {
    double re;
    double im;
public:
    complex(double r, double i);
};
complex createComplex(double r, double i) {
    return {r,i};
}
complex c = createComplex(1,2);     // OK
complex createComplex(double d) {
    return d;
}
complex d = createComplex(2);       // OK
```
The above is valid as `complex` class provides a constructor that can take 2 args. Compiler allows implicit conversion from initializer list to `complex` type.

### Pure virtual function
Pure virtual functions are defined like this, with a *`= 0`* at end  
`virtual double& operator[](int) = 0 `  
Pure virtual function enforces that any deriving class MUST override this function. If it does not override this, it should itself also be declared as pure virtual function.  
Consequently, any class that contains a pure virtual function cannot be used as a concrete class. Such a class is called abstract class  
C++ does not have an `abstract` keyword.

### Virtual table
Function addresses are normally resolved at compile time. Compiler replaces each function call with relocatable machine address to where execution needs to jump. This is, however, not true for virtual function. Take the following example
```cpp
#include <iostream>
using namespace std;

class A {
public:
  virtual void f() {
    cout << "Inside A" << endl;
  }
};

class B: public A {
public:
  void f() {
    cout << "Inside B" << endl;
  }
};

class C: public B {
public:
  void f() {
    cout << "Inside C" << endl;
  }
};

void callf(A &ref) {
  ref.f();
}

int main() {
  C obj;
  callf(obj);
}
```

When `f()` is called on `ref` (in `callf()` function), there is no way for compiler to know which version of `f()` to call. So compiler creates a virtual table for each class in `A`'s hierarchy. Instead of inserting a jump instruction with function's address, compiler delayes determination of function's address to runtime. To facilitate this, each object of all classes in `A`'s hierarchy now have an additional **vptr** in each object, which points to **vtbl** of its class. This is an additional pointer, which is hidden from user, but it's effect can be seen on object's size. This pointer is the placed first in object before any members, so it can be accessed easily. So, when `f()` is called on `ref`, runtime looks for *vptr*. Since the reference actually points to the object of `class C`, the *vptr* points to *vtbl* of its own class, and from there, it can resolve the function address. *vtbl* is a mapping of function name to machine address at which the function code is located.

`getline` method reads a line from stream. Reads until *delimiter* is encountered.
```cpp
string s;
getline(cin, s);   // delimiter is '\n'
getline(cin, s, ',');  
```
It returns numbers of characters read. `getline` reads upto the delimiter, but discards it and doesn't store it in the string.

***
When specifying default parameter for a function, it should be specified in function declaration rather than function definition.

***
### auto_ptr
`auto_ptr` is an older era smart pointer (before C++11). It ensures there's only 1 copy of the object managed by `auto_ptr`.  
Assigning or copying an `auto_ptr` to another takes away the ownership of underlying object away from 1st. The internal pointer in original `auto_ptr` is set to `NULL`.
Once the `auto_ptr` leaves the local scope, the underlying object's destructor is automatically called.  
`auto_ptr` uses `delete` to release the underlying object, hence `auto_ptr` cannot be used with dynamically allocated arrays or memory allocated via `malloc/calloc/realloc`.
```cpp
auto_ptr<int> ptr1 (new int(100));
auto_ptr<int> ptr2 = new int(200);  // ERROR: raw pointer cannot be assigned, the constructor is explicit
auto_ptr<int> ptr3 (ptr1);  // now ptr1 does not own the int allocated on heap
cout << *ptr1 << endl;  // Segfault
cout << *ptr3 << endl;  // OK: 100
*ptr3 = 200;            // OK
```
`auto_ptr` cannot be used with most STL algorithms and containers, as the objects stored in STL containers need to be *copyable*, which `auto_ptr` is not.
The dereference operator is overloaded to directly return the underlying object (not underlying pointer). The returned object can be changed, as the returned reference is not `const`

***
Pass-by-value is generally not a good idea with objects. Let `B` be a base class and `D` be a derived class. We can call a function `fun` which takes `B` object as a parameter, with an object of type `D`. The resulting local object is not same as passed argument, as copy constructor of `B` is invoked. In case `B` is an abstract base class, this will be an error. Also, we cannot take advantage of run-time polymorphism with *pass-by-value* parameters. It is always better to pass in `const` reference, unless a local copy is truly required.  
Another obvious disadvantage is memory/CPU cycles wasted due to copying of object.

***
#### Streams `tie` operation  
`tie` operation ensures that the output buffer is flushed on any subsequent input operations.
```cpp
in.tie(&out);
```
By default, `cin` and `cout` are tied to each other. Hence, any invokation of `cin >>` flushes output buffer.

***  
`a > b` does *not* necessarily mean that `a <= b`. For example, when one of these is `NaN`.
```cpp
double a = std::nan("");    // don't worry about the function.
assert(a > 0 == !(a <= 0)); // assertion fails.
```
---
#### `static` member variables
static members need to be declared in class definition, and defined in the source file.
```cpp
// in header
class A {
    // static int x = 10;  // error, as static member variable cannot be 
                           //initialized at declaration.
    static int count_objects;
public:
    inline static int getCount() {return count_objects};
};
// int A::count_objects = 0;    // will cause linker error if this header is
                                // included in 2 or more source files.
                                // initialization not strictly required.

// in source
int A::count_objects = 0;   // correct; linker error if not defined in source file.
```

In general, static member initialization cannot be done at point of declaration. There is one exception to it. `static const int` type fields can be intialized at the point of definition, without the requirement to define/initialize them again in source file.

For a tempatized class, define directly in the header. Compiler will ensure that one concrete definition is generated for the templatized version
```cpp
template <typename T>
class A<T> {
    static T default_object;
};
T A::default_object;    // definition in the same header file.
```



